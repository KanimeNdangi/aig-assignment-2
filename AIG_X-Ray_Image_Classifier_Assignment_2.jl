### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ 74f67140-d1f6-11eb-025f-332f27db93ea
using Flux: onehotbatch, onecold, crossentropy, throttle, params, argmax

# ╔═╡ a40eeb07-5663-4a20-a927-a49106aff7b5
using Flux: Data.DataLoader, Descent, ADAM, flatten, maxpool

# ╔═╡ 44290935-bb20-4409-935d-d6190eda88b4
using Flux: @epochs

# ╔═╡ 8a7fee1d-4093-4e40-ab18-923f817d978a
using Flux

# ╔═╡ 721b9c9e-d004-4779-a8b0-1728178eab44
using Flux: Conv, Dense, Chain, relu, softmax, CUDA

# ╔═╡ ee594adf-5518-45b9-9790-014ec45a6cd9
using Statistics

# ╔═╡ 807ded61-5ddd-474d-8d01-9db18127c814
using Images

# ╔═╡ 4431fca4-92c3-4881-9aa3-72554e30a043
using Base.Iterators: repeated, partition

# ╔═╡ e86cb35a-d03c-4d9e-8601-7dde5733d421
using Printf

# ╔═╡ 7880d01c-04e8-4f87-ae54-83af66c57fac
using Lathe.preprocess: TrainTestSplit

# ╔═╡ 917509e0-76c6-4eb0-90e3-c70bb6e8bdce
using DataFrames

# ╔═╡ 519d78ef-cf58-46ca-aead-753a89f60dea
using Lathe

# ╔═╡ a8c9e13c-a2f6-4dbc-9909-ebf422a9c879
using MultivariateStats

# ╔═╡ 83b656b9-c7aa-438d-9035-c31f20437d29
using RDatasets

# ╔═╡ ecad4831-f97b-4c6f-aae6-db18883ff88b
using DataStructures

# ╔═╡ 396b7312-6903-4751-a487-a229c8c7fa34
using MLBase

# ╔═╡ 295cb1bb-ba42-48c1-9bf4-ca0b99766b92
using Plots

# ╔═╡ fb655dde-7df6-473c-9e1b-9161554fdeed
using Distances

# ╔═╡ bcab6bda-d397-426e-b57c-bd8219bdf48b
using GLMNet

# ╔═╡ 3127470d-c334-4d1c-ab5b-b08798648187
md"### Load the training and test data"

# ╔═╡ 991a3045-d30c-440a-90ae-58b7f972d23e
data = readdir("C:\\Users\\Ndangi\\Documents\\archive\\chest_xray\\train");

# ╔═╡ 3938f831-6a22-42b4-8c88-76f08269a667
train_norm = readdir("C:\\Users\\Ndangi\\Documents\\archive\\chest_xray\\train\\Normal");

# ╔═╡ 11f3f98a-36b6-406d-8b64-4fd36003be87
train_pneum = readdir("C:\\Users\\Ndangi\\Documents\\archive\\chest_xray\\train\\PNEUMONIA");

# ╔═╡ 51e45ec8-45eb-4605-919e-f483b90654f1
test_norm = readdir("C:\\Users\\Ndangi\\Documents\\archive\\chest_xray\\test\\Normal");

# ╔═╡ 20da8e1c-eea8-47cf-9551-4a662ca80ace
size(test_norm)

# ╔═╡ 68f26a9b-19ed-4551-8829-3a20a85de6ef
test_pneum = readdir("C:\\Users\\Ndangi\\Documents\\archive\\chest_xray\\test\\PNEUMONIA");

# ╔═╡ 179d718f-7e9a-4c08-be9b-7fe5af046b7e
test_pneum[3]

# ╔═╡ 5c3730e1-2bcc-484e-a6a9-f1a273ef6def
typeof(test_pneum[3])

# ╔═╡ 572676c0-7ce5-46d4-9ced-ae20008e6462
train_data = [train_norm; train_pneum];

# ╔═╡ 694536ec-7939-48e6-a2bd-e438ea0b5e09
train_labels = [fill(0, size(train_norm)); fill(1, size(train_pneum))];

# ╔═╡ f6bafc9f-953f-4730-b14b-fbe5b05befda
mb_idxs = partition(1:length(train_data), 10) 

# ╔═╡ df6f9e2b-49d2-4041-8b4b-6293044aec16
#train_set = [transform_rawinputs(train_labels, train_labels, 1) for i in mb_idxs]

# ╔═╡ b4ca0461-e68d-4d88-ac7b-d2b353d16463
md"### Create DataFrames for the data"

# ╔═╡ cab181d2-01a4-47ba-b09b-ac40e54f1e02
df = DataFrame(:Bool => false, :Height => rand(1341), :Width => rand(1341), :Healthy => rand(1341), :Active => rand(1341),  :Feature => train_norm, :Target => train_norm, :Binary => 0);

# ╔═╡ 53b09e05-03be-4a96-9384-0f13193a77b8
dftest = DataFrame(:Bool => false, :Height => rand(264), :Width => rand(264), :Binary => 0);

# ╔═╡ dba395af-9c8f-4039-9abd-d2f2f48fb892
df2 = DataFrame( :Bool => true, :Height => rand(3875), :Width => rand(3875), :Viral => rand(3875), :Bacterial => rand(3875), :Input => train_pneum, :Variables => train_pneum, :Binary => 1);

# ╔═╡ 329fb462-1307-40bc-ab61-9ea2bd17aa69
df2test = DataFrame(:Bool => true, :Height => rand(390), :Width => rand(390), :Binary => 1);

# ╔═╡ 0eee7465-aaf2-4b0c-ad17-f686b5372d0d
size(df2test)

# ╔═╡ 4febe517-9016-41bc-b60c-613f3dc18ad5
df_data = [DataFrame(:Bool => false, :Height => rand(1341), :Width => rand(1341), :Binary => 0);DataFrame( :Bool => true, :Height => rand(3875), :Width => rand(3875), :Binary => 1)];

# ╔═╡ da1f0929-0bac-412f-9a2c-a87c9a6ba949
df_test = [dftest; df2test];

# ╔═╡ 3e73c4ea-4c4a-4e99-9772-b61b420226c7
md"### Extract the matrix"

# ╔═╡ 77db9292-2d60-4117-968b-5c41d650ef7a
X_norm = Matrix(df[:,:]);

# ╔═╡ 158bddbe-ef41-4669-ba5e-7421d3238ad8
X_testnorm = Matrix(dftest[:,:]);

# ╔═╡ e1df4eec-8dde-4cd6-8944-5803e272da48
X_pneum = Matrix(df2[:,:]);

# ╔═╡ 5968faf3-7774-4845-89dd-93e779b31c41
X_testpneum = Matrix(df2test[:,:]);

# ╔═╡ 1814d941-2858-48dd-b35e-01a3373688a1
X_data = Matrix(df_data[:,:]);

# ╔═╡ ada5ed39-f183-419a-aad4-a8b5fd31ccee
X_test = Matrix(df_test[:,:]);

# ╔═╡ 445e35b4-5276-4e04-99f5-46b9176d9aff
md"### Labels"

# ╔═╡ 0cfba3aa-0b3a-4911-9720-0e802b76be54
# Create labels foor the data
norm_labels = X_norm[:,:];

# ╔═╡ 63b2e5c5-e944-41d7-8324-0f97138bffa4
norm_testlabels = X_test[:,:];

# ╔═╡ 6ef19770-a1be-4eda-aa09-5b3c62b52a42
pneum_labels = X_pneum[:,:];

# ╔═╡ 7a0211cb-f6c8-42db-91e7-6f7941f77d32
pneumtest_labels = X_testpneum[:,:];

# ╔═╡ 88413550-3821-4f99-8188-e5c29d87e89f
data_labels = X_data[:,:];

# ╔═╡ f28ca3f0-49e1-4c5b-b041-f85b6015e6b0
test_labels = X_test[:,:];

# ╔═╡ 8e5d80d8-05c1-481f-a1ee-a1cf70e7d23b
md"###  Labelmaps for data"

# ╔═╡ 2a1764a5-02a5-4bdc-9476-178dce41a19a
# Create label maps for the data
normlabelsmap = labelmap(norm_labels);

# ╔═╡ 0d615d24-c830-4eee-be75-ed00fa3001ca
xtestlabelsmap = labelmap(norm_testlabels);

# ╔═╡ 98c67f89-ec20-4433-ac44-c36a78cd005b
pneumlabelsmap = labelmap(pneum_labels);

# ╔═╡ 7126a6bd-3f57-4ed1-8802-09f0bdf64d02
pneumtestlabelmap = labelmap(pneumtest_labels);

# ╔═╡ 25a6ac6c-9710-4a19-8bb3-6a833043d320
datalabelsmap = labelmap(data_labels);

# ╔═╡ b8c19eb8-dfe1-4100-8c27-ee6163be7b2a
testlablemap = labelmap(test_labels);

# ╔═╡ d216a64e-bc35-4c76-9aad-de177971867f
md"### Label encoding"

# ╔═╡ 1bfb3003-abee-40f5-a95e-385601a7d3d0
# Label encode the normal train and test data
ynorm = labelencode(normlabelsmap, norm_labels);

# ╔═╡ 554024e0-23f2-4d06-a57b-13516a017c3b
ynormtest = labelencode(xtestlabelsmap, norm_testlabels);

# ╔═╡ fa931310-4b5c-4b7a-b977-e6d66fd83d03
# Label encode the pneumonic train and test data
ypneum = labelencode(pneumlabelsmap, pneum_labels);

# ╔═╡ cb76320f-c512-4ce4-8c97-828ed9435c0f
ypneumtest = labelencode(pneumtestlabelmap, pneumtest_labels);

# ╔═╡ a0dfe10a-9141-41ba-9e1c-029352e4be1c
datamap = labelencode(datalabelsmap, data_labels);

# ╔═╡ 8554935a-cd3d-41fe-96e2-9f4c845c29f4
testmap = labelencode(testlablemap, test_labels);

# ╔═╡ d8365398-28a7-4b98-b0d8-1c5d1c801b57
md"### Split function"

# ╔═╡ 06bd4243-0ec6-432e-919f-b9376312af78
dataids = TrainTestSplit(datamap[:], 0.8);

# ╔═╡ bb963895-7365-4155-a787-1cae442b675a
test_ids = TrainTestSplit(testmap[:], 0.8);

# ╔═╡ bd2ceadb-db5c-4b3b-a8e4-1f45cb6f9488
testids = setdiff(1:length(datamap));

# ╔═╡ bcd022fe-bebc-4f9b-85e9-04b606836953
# TrainTestSplit  function that separates the data into the training and testin sets
trainids = TrainTestSplit(ynorm[:], 0.8 );

# ╔═╡ aa2d3fc4-44df-453a-a118-59d90eeea19c
testidsa = setdiff(1:length(ynorm));

# ╔═╡ 0663eb98-ea43-44e3-b1e8-06aca97cded3
trainidsx = TrainTestSplit(ypneum[:], 0.8 );

# ╔═╡ 365080db-e71f-4d72-b02b-a8d4ac4a6337
testidsx = setdiff(1:length(ypneum));

# ╔═╡ 3f0ebf76-0643-480e-bca0-4fd078e1ffdb
md"###  Predicted value assignment"

# ╔═╡ 0a59fb6a-55c2-4d33-a464-fc8c71c88edc
assign_class(predictedvalue) = argmin(abs.(predictedvalue .- [1,2,3]));

# ╔═╡ 8a962d3f-3a10-45e1-807a-578c516d15f1
# Query the train and test data
qdata = X_data[datamap]

# ╔═╡ 6a75def5-53f5-440b-bdaa-71b78a69da78
qtest  = X_test[testmap]

# ╔═╡ 8211acf1-49ae-4929-9086-dc43803a5f37
md"### Vectorization"

# ╔═╡ 41361956-1de8-4c3b-8daa-b8733fda382b
# Normal train and test vectors
norm_vec = Vector{Int64}(undef, size(ynorm)[1]);

# ╔═╡ 5a638fed-ce19-4997-a186-381ed47fd27f
testnorm_vec = Vector{Int64}(undef, size(ynormtest)[1]);

# ╔═╡ 7faf9cd0-1b3c-4894-a497-7a5d40caa55a
typeof(norm_vec)

# ╔═╡ 217688da-60e9-490d-b65a-3594cee40163
# Pneumonia train and test vectors
pneum_vec = Vector{Int64}(undef, size(ypneum)[1]);

# ╔═╡ 1992179e-fc6c-4893-abd1-7187af1d6715
testpneum_vec = Vector{Int64}(undef, size(ypneumtest)[1]);

# ╔═╡ 6823940b-6494-4eb8-b900-38ca249535ef
# Data and test vectors
data_vec = Vector{Int64}(undef, size(datamap)[1]);

# ╔═╡ 6cfb01ec-d391-4357-bc6c-81e0cb6cbbb4
test_vec = Vector{Int64}(undef, size(testmap)[1]);

# ╔═╡ 67350f70-c873-43de-a367-b27cae90705e
md"### Dataloader for train"

# ╔═╡ 6a94efbd-9fc8-492f-9854-7278f406dd31
train_norms = DataLoader(norm_vec, batchsize = 128);

# ╔═╡ fa9d4b40-b906-4cbc-8761-54215dcde033
train_pneums = DataLoader(pneum_vec, batchsize = 128);

# ╔═╡ b950ac9d-3c24-46f6-b8b5-e590f561c82d
data_train = DataLoader(data_vec, batchsize = 128);

# ╔═╡ b58c7522-ac24-4212-b4d7-a71d07858a04
md"### Defining Model Architecture"

# ╔═╡ cfc7e32f-8ee9-4c44-a6ac-f9a5a27d3928
#= Build model
function model(imagesize = (28,28,1), nrclasses = 10)
    output_size = Int.(floor.([imagesize[1]/8,imagesize[2]/8,32]))
    
    return Chain(
        Conv((3, 3), imagesize[3]=>16, pad=(1,1), relu),
        maxpool((2,2)),
    
        Conv((3, 3), 16=>32, pad(1,1), relu),
        maxpool((2,2)),
        
   
        Conv((3, 3), 32=>32, pad=(1,1), relu),
        maxpool((2,2)),
        
        flatten,
        Dense(prod(output_size), 10)
    )
    end=#

# ╔═╡ bf73ad7b-474a-4e87-b7dc-2f8e4366d2c9
md"## LeNet 5 Convolutional Neural Network"

# ╔═╡ 745fafff-7b1e-4e23-b057-4d048df6cd5b
model = Chain(
# First convolutional layer, works on a 28x28 image
Conv((3, 3), 1=>16, pad=(1,1), stride=2, relu),
    x -> maxpool(x, (2,2)),
# Second convolutional layer, works on a 14x14 image
Conv((3, 3), 16=>32, pad=(1,1), stride=2, relu),
    x -> maxpool(x, (2,2)),
 # Third convolutional layer, works on a 7x7 image
Conv((3, 3), 32=>32, pad=(1,1), stride=2, relu),
# Reshape the the fouth layer(3D tensor) into a 2D one using Flux.flatten
    # at this point it should be a (3, 3, 32, N)
Conv((3, 3), 32=>64, pad=(1,1), stride=2, relu),
    x -> reshape(x, :, size(x, 1)),
    flatten,
# Average pooling on each width x height feature map
Dense(2, 1), softmax)

# ╔═╡ 6666b65d-afde-4019-a470-933cc9f4a2ac
# In general model is the function containing functions that define the LeNet 5 CNN
# Contains a "Chain" representing transformations in the network
# The chain has 3 convolutions which reduce the size of the dims, followed by a Dense layer
# The Dense layer is the "neuron" of the network
# Conv((3,3)defines size of os convolution, works 3x3 pixels, it's 2D for 2D images
# imagesize[3]=>16, defines input and output channels. imagesize[3] is just 1, takes input with 1 channel
# And produces an output with 16 channels.
# pad=(1,1) means sides of input will fill up before convolution, so first two dims of resut will = size input
# If we used (5, 5) convolution, then we need pad=(2,2)
# relu means sigma activatin is rectified linear unit or takes 28x28x1 image and produces 28x28x16 activation map
# Maxpool((2,2)) reduces the size of out put and uses "2x2" windows

# ╔═╡ ae3b5853-0fe0-4783-ad9a-b184269500b3
md"##  Utility Functions"

# ╔═╡ d46dc2bc-c8ff-499c-b3f8-2700adeb7fff
md"### Loss function"

# ╔═╡ 06586cb9-2bf1-4666-afae-301653f911c2
# Loss
# calculates the crossentropy loss between prediction yhat
# from model(x) and the ground truth y.
# augment data a bit adding gaussian noise to image for robust

# ╔═╡ 776e6fe6-c755-4417-8723-2057f9cf65dd
md"### Loss function to return a number showing how far the model is from its target"

# ╔═╡ c50fc082-d305-4b93-817c-911552456a23
# Model upgrade, optimization and performance checking
loss(x, y) = Flux.crossentropy(model(x), y)

# ╔═╡ b58cf77c-83dd-46ff-8bde-f05179103815
accuracy(x, y) = mean(argmax(model(x)) .== argmax(y))

# ╔═╡ 1f7dfcc0-8c12-43ba-b163-e992a9b6e822
lr = 0.1

# ╔═╡ feb2f823-cd10-4536-8573-524cdb458018
md"### Select an Optimizer"

# ╔═╡ 1dc1510a-ca61-4756-a546-5aadc34bfe5e
# Selecting an optimizer
# Train our model with the given training set using the ADAM optimizer
# Print out performance against the testset
# Controls how model updates at each iteration

# ╔═╡ 3c36d497-9413-48b8-85ce-685380aeca5e
optimizer = Descent(lr)

# ╔═╡ 17414287-a1ef-45a2-87ff-f17078a042e4
number_epochs = 10

# ╔═╡ f03c6ea3-84fd-416a-88dd-7fd2f0a1087c
typeof(train_norms)

# ╔═╡ ef39637d-04da-4ac7-b87f-c23e53d9b693
function transform_batch(X, idxs)
    X_batch = Array{Float32}(undef, size(X[1])..., 1, length(idxs))
    for i in 1:length(idxs)
        X_batch[:, :, :, i] = Float32.(X[idxs[i]])
    end
    return (X_batch)
end

# ╔═╡ f61cf9d8-133c-475f-873b-d561b9a6699c
batch_size = 128

# ╔═╡ 3211b48c-b56b-456a-900b-411cf9e4b321
#floatingpoint_image = float.(testids)
#floatingpoint_image2 = float.(testidsx)

# ╔═╡ 19bf1312-fc22-442a-88d8-3efe1f1dfc7c
X = hcat(data_vec...)

# ╔═╡ 8459c095-4e66-46e4-9322-ed19ea0d51e8
X2 = hcat(data_vec...)

# ╔═╡ 41400aae-826f-4ddf-b5df-43809d0767d8
md"### Optimizaton function"

# ╔═╡ 2beb0bcf-6dfc-4579-937d-822316fdb7e1
opt = ADAM()

# ╔═╡ bb4b99ee-af05-4dfd-a0f3-8ae15b619a9d
md"### Objective function hat evaluates the performance of my model"

# ╔═╡ 0dd69084-8ed8-438b-930b-bff06cd630c6
evalcb() = @show(loss(data_vec, train_data))

# ╔═╡ 4345ed8f-8e76-424a-b84f-c38340869458
md"### parameters/ datapoints to be provided to the objective function"

# ╔═╡ 702d4b94-4f90-416a-8e8e-2e7bb042c27a
ps = Flux.params(model) ;

# ╔═╡ 4a9bbd31-9641-4136-bb43-e165aee7a255
md"### The datasetx provides a collection of data to train with"

# ╔═╡ a66730b8-f39c-428a-8423-f82078cab011
datasetx = repeated((qdata, data_vec),200);

# ╔═╡ 7ad29f76-66ca-4abf-a203-c5ae8c682631
#Flux.train!(loss, ps, datasetx, optimizer, cb = throttle(evalcb, 10))

# ╔═╡ 9f865ee1-f73c-4f44-a04f-72b078f9d36d
function accuracy1(train_norms, model)
    acc = 0
    for (x,y) in train_norms
        @show model(x)      # **HERE : MODEL ALLWAYS THE SAME** 
        acc += sum(onecold(cpu(model(x))) .== onecold(cpu(y)))*1 / size(x,2)
    end
    acc/length(train_norms);
end

# ╔═╡ 63deeaa4-57a2-40c9-ac2a-1484ead83835
#accuracy1(data_vec[1], test_vec)

# ╔═╡ 089e2aff-4a76-4867-be3c-05362231dbf8
#testX_data = hcat(float.(reshape.(qdata, 1:128))...)

# ╔═╡ ce39ba3f-0404-4ef1-b697-0ac06628930a
#test_image = model(data_vec, qdata)

# ╔═╡ 3b83e0cb-e0cb-472e-81b2-3b87fa6753ab
dataset = repeated((testidsx), 200)

# ╔═╡ b891f7fb-e754-4b55-bb28-07723b201034
md"## Scaling parameters and so forth"

# ╔═╡ 2699d2b9-5ac4-4c7c-b9ab-49f0b5bc8762
function scaling_parameters(init_feature_mat)
    feat_mean = mean(init_feature_mat, dims=1)
    feat_dev = std(init_feature_mat, dims=1)
    return (feat_mean, feat_dev)
end

# ╔═╡ 2dc8f79d-6d65-4b89-92cf-82970fb6dd20
scaling_params = scaling_parameters(datamap)

# ╔═╡ 7c567583-2b93-49ae-a7b4-75041d2081ee
scaling_params2 = scaling_parameters(datamap)

# ╔═╡ bcf1aff4-4f94-484a-ad20-df0bea73ff92
function scale_features(feature_mat, sc_params)
scaled_feature_mat = (feature_mat .- sc_params[1]) ./ sc_params[2]
end

# ╔═╡ 8c63ea64-6e69-45e3-8468-52f5b44e3888
scaled_training_features = scale_features(datamap, scaling_params)

# ╔═╡ ff861c07-6d3f-4d77-9d8e-0e86b2338e67
scaled_testing_features = scale_features(testmap, scaling_params)

# ╔═╡ 67af23f3-65b5-4bf2-9fc8-0c0e7846c076
md"### Sigmoid function for extra cautiousness in case I floppy disk"

# ╔═╡ 8d0c5b1b-0151-41d6-92cb-5856433fdd6c
function sigmoid(z)
1 ./ (1 .+ exp.(.-z))
end

# ╔═╡ 213dff43-eabb-45c6-b82a-ed8da88e562a
function cost(aug_features, outcome, weights, reg_param)
    sample_count = length(outcome)
    hypothesis = sigmoid(aug_features * weights)
    cost_part_1 = ((-outcome)' * log.(hypothesis))[1]
    cost_part_2 = ((1 .- outcome)' * log.(1 .- hypothesis))[1]
    lambda_regul = (reg_param/(2 * sample_count) * sum(weights[2:end] .^ 2))
    error = (1/sample_count) * (cost_part_1 - cost_part_2) + lambda_regul
    grad_error_all = ((1/sample_count) * (aug_features') * (hypothesis - outcome)) +
    ((1/sample_count) * (reg_param * weights))
    grad_error_all[1] = ((1/sample_count) * (aug_features[:,1])' * (hypothesis - outcome))[1]
    return(error, grad_error_all)
end

# ╔═╡ a42b1892-bddb-486c-94a3-16a78ab54308
md"#### Alternative training model since flux is finna flunk me"

# ╔═╡ 759db953-7afd-4180-a708-430eddba4496
function train_model(features, outcome, reg_param, alpha, max_iter)
    sample_count = length(outcome)
    aug_features = hcat(ones(sample_count,1), features)
    feature_count = size(aug_features)[2]
    weights = zeros(feature_count)
    errors = zeros(max_iter)
    for i in 1:max_iter
        error_and_grad_tp = cost(aug_features, outcome, weights, reg_param)
        errors[i] = error_and_grad_tp[1]
        weights = weights - (alpha * error_and_grad_tp[2])
    end
    return (errors, weights)
end

# ╔═╡ 34c6c6c5-264a-491d-9faa-7ee39c06977b
training_weight_errors = train_model(scaled_training_features, data_vec, 0.001, 0.2,700)

# ╔═╡ dc33cef1-97f5-4d1c-abe7-9f5e0fd52114
plot(training_weight_errors[1],label="Cost",ylabel="Cost",xlabel="Number of Iteration",
title="Cost Per Iteration")

# ╔═╡ 4e5ea67c-e018-4764-8029-9760656f3ae2
function predictions(features, weights)
total_entry_count = size(features)[1]
aug_features = hcat(ones(total_entry_count, 1), features)
preds = sigmoid(aug_features * weights)
return preds
end

# ╔═╡ 33892b6c-daf0-4fee-ada7-fee399821ca8
function predicted_classes(preds, threshold)
return preds .>= threshold
end

# ╔═╡ 597279ff-9f13-4017-8427-8b18d7a9fb29
test_predicted_cls = predicted_classes(predictions(scaled_testing_features, training_weight_errors[2]), 0.5)

# ╔═╡ 37109fc6-1e80-43a7-bdf9-ff125d39b67d
md"### Confusion Matrix"

# ╔═╡ e6001584-c5e0-4307-84da-3a4e317e0dc5
function confusion_matrix(data_vec, lb)
  plb = Flux.onehotbatch(Flux.onecold((norm_vec)), 1:length(data_vec))
  lb * plb'
end

# ╔═╡ 68e822ba-f2b8-4948-8e80-730490f66650
confusion_matrix(test_vec, data_vec)

# ╔═╡ e25f84c3-12df-45eb-8a9f-98e7fdf57a03
train_conf_matrix = confusion_matrix(data_vec, test_predicted_cls)

# ╔═╡ c670e913-dff9-4616-881c-625348d22a01
test_conf_matrix = confusion_matrix(testnorm_vec,  test_predicted_cls)

# ╔═╡ 51e770ab-0f93-414d-a260-1fc1397a73e2
#accuracy1(test_conf_matrix)

# ╔═╡ c599efb3-70a8-4325-b553-6413baf28f0f
#recall(test_conf_matrix)

# ╔═╡ 6351c5fb-fcea-4077-8fc5-dfd1a92b989c
#precision(test_conf_matrix)

# ╔═╡ f2ec79a3-0fec-4457-b1e1-eeefaae10c4f
md"### End of assignment"

# ╔═╡ Cell order:
# ╠═74f67140-d1f6-11eb-025f-332f27db93ea
# ╠═a40eeb07-5663-4a20-a927-a49106aff7b5
# ╠═44290935-bb20-4409-935d-d6190eda88b4
# ╠═8a7fee1d-4093-4e40-ab18-923f817d978a
# ╠═721b9c9e-d004-4779-a8b0-1728178eab44
# ╠═ee594adf-5518-45b9-9790-014ec45a6cd9
# ╠═807ded61-5ddd-474d-8d01-9db18127c814
# ╠═4431fca4-92c3-4881-9aa3-72554e30a043
# ╠═e86cb35a-d03c-4d9e-8601-7dde5733d421
# ╠═7880d01c-04e8-4f87-ae54-83af66c57fac
# ╠═917509e0-76c6-4eb0-90e3-c70bb6e8bdce
# ╠═519d78ef-cf58-46ca-aead-753a89f60dea
# ╠═a8c9e13c-a2f6-4dbc-9909-ebf422a9c879
# ╠═83b656b9-c7aa-438d-9035-c31f20437d29
# ╠═ecad4831-f97b-4c6f-aae6-db18883ff88b
# ╠═396b7312-6903-4751-a487-a229c8c7fa34
# ╠═295cb1bb-ba42-48c1-9bf4-ca0b99766b92
# ╠═fb655dde-7df6-473c-9e1b-9161554fdeed
# ╠═bcab6bda-d397-426e-b57c-bd8219bdf48b
# ╟─3127470d-c334-4d1c-ab5b-b08798648187
# ╠═991a3045-d30c-440a-90ae-58b7f972d23e
# ╠═3938f831-6a22-42b4-8c88-76f08269a667
# ╠═11f3f98a-36b6-406d-8b64-4fd36003be87
# ╠═51e45ec8-45eb-4605-919e-f483b90654f1
# ╠═20da8e1c-eea8-47cf-9551-4a662ca80ace
# ╠═68f26a9b-19ed-4551-8829-3a20a85de6ef
# ╠═179d718f-7e9a-4c08-be9b-7fe5af046b7e
# ╠═5c3730e1-2bcc-484e-a6a9-f1a273ef6def
# ╠═572676c0-7ce5-46d4-9ced-ae20008e6462
# ╠═694536ec-7939-48e6-a2bd-e438ea0b5e09
# ╠═f6bafc9f-953f-4730-b14b-fbe5b05befda
# ╠═df6f9e2b-49d2-4041-8b4b-6293044aec16
# ╟─b4ca0461-e68d-4d88-ac7b-d2b353d16463
# ╠═cab181d2-01a4-47ba-b09b-ac40e54f1e02
# ╠═53b09e05-03be-4a96-9384-0f13193a77b8
# ╠═dba395af-9c8f-4039-9abd-d2f2f48fb892
# ╠═0eee7465-aaf2-4b0c-ad17-f686b5372d0d
# ╠═329fb462-1307-40bc-ab61-9ea2bd17aa69
# ╠═4febe517-9016-41bc-b60c-613f3dc18ad5
# ╠═da1f0929-0bac-412f-9a2c-a87c9a6ba949
# ╠═3e73c4ea-4c4a-4e99-9772-b61b420226c7
# ╠═77db9292-2d60-4117-968b-5c41d650ef7a
# ╠═158bddbe-ef41-4669-ba5e-7421d3238ad8
# ╠═e1df4eec-8dde-4cd6-8944-5803e272da48
# ╠═5968faf3-7774-4845-89dd-93e779b31c41
# ╠═1814d941-2858-48dd-b35e-01a3373688a1
# ╠═ada5ed39-f183-419a-aad4-a8b5fd31ccee
# ╟─445e35b4-5276-4e04-99f5-46b9176d9aff
# ╠═0cfba3aa-0b3a-4911-9720-0e802b76be54
# ╠═63b2e5c5-e944-41d7-8324-0f97138bffa4
# ╠═6ef19770-a1be-4eda-aa09-5b3c62b52a42
# ╠═7a0211cb-f6c8-42db-91e7-6f7941f77d32
# ╠═88413550-3821-4f99-8188-e5c29d87e89f
# ╠═f28ca3f0-49e1-4c5b-b041-f85b6015e6b0
# ╠═8e5d80d8-05c1-481f-a1ee-a1cf70e7d23b
# ╠═2a1764a5-02a5-4bdc-9476-178dce41a19a
# ╠═0d615d24-c830-4eee-be75-ed00fa3001ca
# ╠═98c67f89-ec20-4433-ac44-c36a78cd005b
# ╠═7126a6bd-3f57-4ed1-8802-09f0bdf64d02
# ╠═25a6ac6c-9710-4a19-8bb3-6a833043d320
# ╠═b8c19eb8-dfe1-4100-8c27-ee6163be7b2a
# ╠═d216a64e-bc35-4c76-9aad-de177971867f
# ╠═1bfb3003-abee-40f5-a95e-385601a7d3d0
# ╠═554024e0-23f2-4d06-a57b-13516a017c3b
# ╠═fa931310-4b5c-4b7a-b977-e6d66fd83d03
# ╠═cb76320f-c512-4ce4-8c97-828ed9435c0f
# ╠═a0dfe10a-9141-41ba-9e1c-029352e4be1c
# ╠═8554935a-cd3d-41fe-96e2-9f4c845c29f4
# ╟─d8365398-28a7-4b98-b0d8-1c5d1c801b57
# ╠═06bd4243-0ec6-432e-919f-b9376312af78
# ╠═bb963895-7365-4155-a787-1cae442b675a
# ╠═bd2ceadb-db5c-4b3b-a8e4-1f45cb6f9488
# ╠═bcd022fe-bebc-4f9b-85e9-04b606836953
# ╠═aa2d3fc4-44df-453a-a118-59d90eeea19c
# ╠═0663eb98-ea43-44e3-b1e8-06aca97cded3
# ╠═365080db-e71f-4d72-b02b-a8d4ac4a6337
# ╟─3f0ebf76-0643-480e-bca0-4fd078e1ffdb
# ╠═0a59fb6a-55c2-4d33-a464-fc8c71c88edc
# ╠═8a962d3f-3a10-45e1-807a-578c516d15f1
# ╠═6a75def5-53f5-440b-bdaa-71b78a69da78
# ╟─8211acf1-49ae-4929-9086-dc43803a5f37
# ╠═41361956-1de8-4c3b-8daa-b8733fda382b
# ╠═5a638fed-ce19-4997-a186-381ed47fd27f
# ╠═7faf9cd0-1b3c-4894-a497-7a5d40caa55a
# ╠═217688da-60e9-490d-b65a-3594cee40163
# ╠═1992179e-fc6c-4893-abd1-7187af1d6715
# ╠═6823940b-6494-4eb8-b900-38ca249535ef
# ╠═6cfb01ec-d391-4357-bc6c-81e0cb6cbbb4
# ╠═67350f70-c873-43de-a367-b27cae90705e
# ╠═6a94efbd-9fc8-492f-9854-7278f406dd31
# ╠═fa9d4b40-b906-4cbc-8761-54215dcde033
# ╠═b950ac9d-3c24-46f6-b8b5-e590f561c82d
# ╟─b58c7522-ac24-4212-b4d7-a71d07858a04
# ╠═cfc7e32f-8ee9-4c44-a6ac-f9a5a27d3928
# ╟─bf73ad7b-474a-4e87-b7dc-2f8e4366d2c9
# ╠═745fafff-7b1e-4e23-b057-4d048df6cd5b
# ╠═6666b65d-afde-4019-a470-933cc9f4a2ac
# ╟─ae3b5853-0fe0-4783-ad9a-b184269500b3
# ╟─d46dc2bc-c8ff-499c-b3f8-2700adeb7fff
# ╠═06586cb9-2bf1-4666-afae-301653f911c2
# ╟─776e6fe6-c755-4417-8723-2057f9cf65dd
# ╠═c50fc082-d305-4b93-817c-911552456a23
# ╠═b58cf77c-83dd-46ff-8bde-f05179103815
# ╠═1f7dfcc0-8c12-43ba-b163-e992a9b6e822
# ╟─feb2f823-cd10-4536-8573-524cdb458018
# ╠═1dc1510a-ca61-4756-a546-5aadc34bfe5e
# ╠═3c36d497-9413-48b8-85ce-685380aeca5e
# ╠═17414287-a1ef-45a2-87ff-f17078a042e4
# ╠═f03c6ea3-84fd-416a-88dd-7fd2f0a1087c
# ╠═ef39637d-04da-4ac7-b87f-c23e53d9b693
# ╠═f61cf9d8-133c-475f-873b-d561b9a6699c
# ╠═3211b48c-b56b-456a-900b-411cf9e4b321
# ╠═19bf1312-fc22-442a-88d8-3efe1f1dfc7c
# ╠═8459c095-4e66-46e4-9322-ed19ea0d51e8
# ╠═41400aae-826f-4ddf-b5df-43809d0767d8
# ╠═2beb0bcf-6dfc-4579-937d-822316fdb7e1
# ╠═bb4b99ee-af05-4dfd-a0f3-8ae15b619a9d
# ╠═0dd69084-8ed8-438b-930b-bff06cd630c6
# ╠═4345ed8f-8e76-424a-b84f-c38340869458
# ╠═702d4b94-4f90-416a-8e8e-2e7bb042c27a
# ╟─4a9bbd31-9641-4136-bb43-e165aee7a255
# ╠═a66730b8-f39c-428a-8423-f82078cab011
# ╠═7ad29f76-66ca-4abf-a203-c5ae8c682631
# ╠═9f865ee1-f73c-4f44-a04f-72b078f9d36d
# ╠═63deeaa4-57a2-40c9-ac2a-1484ead83835
# ╠═089e2aff-4a76-4867-be3c-05362231dbf8
# ╠═ce39ba3f-0404-4ef1-b697-0ac06628930a
# ╠═3b83e0cb-e0cb-472e-81b2-3b87fa6753ab
# ╠═b891f7fb-e754-4b55-bb28-07723b201034
# ╠═2699d2b9-5ac4-4c7c-b9ab-49f0b5bc8762
# ╠═2dc8f79d-6d65-4b89-92cf-82970fb6dd20
# ╠═7c567583-2b93-49ae-a7b4-75041d2081ee
# ╠═bcf1aff4-4f94-484a-ad20-df0bea73ff92
# ╠═8c63ea64-6e69-45e3-8468-52f5b44e3888
# ╠═ff861c07-6d3f-4d77-9d8e-0e86b2338e67
# ╟─67af23f3-65b5-4bf2-9fc8-0c0e7846c076
# ╠═8d0c5b1b-0151-41d6-92cb-5856433fdd6c
# ╠═213dff43-eabb-45c6-b82a-ed8da88e562a
# ╟─a42b1892-bddb-486c-94a3-16a78ab54308
# ╠═759db953-7afd-4180-a708-430eddba4496
# ╠═34c6c6c5-264a-491d-9faa-7ee39c06977b
# ╠═dc33cef1-97f5-4d1c-abe7-9f5e0fd52114
# ╠═4e5ea67c-e018-4764-8029-9760656f3ae2
# ╠═33892b6c-daf0-4fee-ada7-fee399821ca8
# ╠═597279ff-9f13-4017-8427-8b18d7a9fb29
# ╟─37109fc6-1e80-43a7-bdf9-ff125d39b67d
# ╠═e6001584-c5e0-4307-84da-3a4e317e0dc5
# ╠═68e822ba-f2b8-4948-8e80-730490f66650
# ╠═e25f84c3-12df-45eb-8a9f-98e7fdf57a03
# ╠═c670e913-dff9-4616-881c-625348d22a01
# ╠═51e770ab-0f93-414d-a260-1fc1397a73e2
# ╠═c599efb3-70a8-4325-b553-6413baf28f0f
# ╠═6351c5fb-fcea-4077-8fc5-dfd1a92b989c
# ╠═f2ec79a3-0fec-4457-b1e1-eeefaae10c4f
